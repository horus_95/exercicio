import java.util.Scanner;

public class Ex1 {
    static String[] lista = new String[50];
    static Scanner tecla = new Scanner(System.in);
    static int index = 0;
    
    
    public static void main(String[] args) {
        int op;
        do {                
            System.out.println("*** MENU PRINCIPAL ***");
            System.out.println("1- Cadastar pessoas");
            System.out.println("2-Listar pessoas");
            System.out.println("3-Sair");
            op = tecla.nextInt();
            switch(op){
                case 1: Cadastrar(); break;
                case 2: Listar(); break;
                case 3: break;
            }
        } while (op!=3);       
    }
    
    public static void Cadastrar(){
        //Entrada
    	tecla.nextLine();
        System.out.println("Digite o nome da pessoa:");
        String nome = tecla.nextLine();
        lista[index++] = new String(nome);
        System.out.println("Nome cadastrado com sucesso!");
    }
    
    public static void Listar(){
        for (int i = 0; i < lista.length-1; i++) {
            if (lista[i] != null){
                System.out.println(lista[i]);
            }else{
                break;
            }
        }
    }
    
}
