public class Produto {

    private final static double TAXA = 8.9;
    
    static int contador = 0;
    
    //Atributos de instancia
    private int numero;
    private double saldo;
    private boolean status;
    
    //Metodos de acesso
    public int getNumero() {
        return numero;
    }

    public void setNumero(int numero) {
        this.numero = numero;
    }

    public double getSaldo() {
        return saldo;
    }

    public void setSaldo(double saldo) {
        this.saldo = saldo;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    //Construtor
    public Produto(int numero, double saldo) {
        this.numero = numero;
        this.saldo = saldo;
        this.status = true;
    }
    
    //Sobrecarregado
    public Produto(double saldo){
    	this.numero = ++contador;
        this.saldo = saldo;
        this.status = true;
    }

    public void sacar(double valor){
        saldo -= valor;
        //saldo = saldo - valor;
    }
    
    public void depositar(double valor){
        saldo += valor;
        //saldo = saldo + valor;
    }
}
